package com.liodevel.liacembdr;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static com.liodevel.liacembdr.Utils.timePickerToLong;

public class EventActivity extends AppCompatActivity {

    // GOOGLE CALENDAR
    GoogleAccountCredential mCredential;
    ContentResolver cr;
    Context context;

    TimePicker timePickerInicio, timePickerFin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_event);

        context = this;
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(Statics.SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(settings.getString(Statics.PREF_ACCOUNT_NAME, null));
        cr = getContentResolver();

        try {

            Intent intent = getIntent();

            // VISTAS

            Button acceptButton = (Button) findViewById(R.id.botonGuardar);

            final EditText titulo = (EditText) findViewById(R.id.titulo);

            final Spinner tipoServicio = (Spinner) findViewById(R.id.spinner_tipo);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.array_tipos_eventos, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tipoServicio.setAdapter(adapter);

            final CheckBox checkHoraExtra = (CheckBox) findViewById(R.id.checkBox_extra);

            timePickerInicio = (TimePicker) findViewById(R.id.horaInicio);
            timePickerInicio.setIs24HourView(true);

            timePickerFin = (TimePicker) findViewById(R.id.horaFin);
            timePickerFin.setIs24HourView(true);

            // Cambiar intervalos de los minutos en los timePickers
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field field = classForid.getField("minute");

                // timePicker Inicio
                NumberPicker mMinuteSpinnerInicio = (NumberPicker) timePickerInicio.findViewById(field.getInt(null));

                mMinuteSpinnerInicio.setMinValue(0);
                mMinuteSpinnerInicio.setMaxValue(3);
                List<String> displayedValuesInicio = new ArrayList<String>();
                displayedValuesInicio.add(0, "00");
                displayedValuesInicio.add(1, "15");
                displayedValuesInicio.add(2, "30");
                displayedValuesInicio.add(3, "45");

                mMinuteSpinnerInicio.setDisplayedValues(displayedValuesInicio.toArray(new String[0]));

                // timePicker Final
                NumberPicker mMinuteSpinnerFin = (NumberPicker) timePickerFin.findViewById(field.getInt(null));

                mMinuteSpinnerFin.setMinValue(0);
                mMinuteSpinnerFin.setMaxValue(3);
                List<String> displayedValuesFin = new ArrayList<String>();
                displayedValuesFin.add(0, "00");
                displayedValuesFin.add(1, "15");
                displayedValuesFin.add(2, "30");
                displayedValuesFin.add(3, "45");

                mMinuteSpinnerFin.setDisplayedValues(displayedValuesFin.toArray(new String[0]));

            } catch (Exception e) {
                Log.e(Statics.DEBUG_TAG, "Error timepickers: " + e.toString());
            }

            Log.d(Statics.DEBUG_TAG, "--GET EXTRAS ");


            final String selectedAccount = intent.getExtras().getString("selected_account");
            Log.d(Statics.DEBUG_TAG, "selected_account: " + selectedAccount);

            final boolean edicion = intent.getExtras().getBoolean("edicion");
            Log.d(Statics.DEBUG_TAG, "edicion: " + edicion);


            // Si es un evento nuevo
            if (!edicion) {

                final int year = intent.getExtras().getInt("year");
                Log.d(Statics.DEBUG_TAG, "year: " + year);

                final int month = intent.getExtras().getInt("month");
                Log.d(Statics.DEBUG_TAG, "month: " + month);

                final int day = intent.getExtras().getInt("day");
                Log.d(Statics.DEBUG_TAG, "day: " + day);

                if (acceptButton != null) {
                    acceptButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //GUARDAR

                            int valorInicio = timePickerInicio.getCurrentMinute();
                            int valorFin = timePickerFin.getCurrentMinute();

                            Log.i("LIA", "MINUTOS_INI: " + valorInicio);
                            Log.i("LIA", "MINUTOS_FIN: " + valorFin);

                            long dateTimeStart = timePickerToLong(
                                    year,
                                    month,
                                    day,
                                    timePickerInicio.getCurrentHour(),
                                    valorInicio * 15);
                            long dateTimeEnd = timePickerToLong(
                                    year,
                                    month,
                                    day,
                                    timePickerFin.getCurrentHour(),
                                    valorFin * 15);

                            WeekViewEvent newEvent = Server.crearEvento(
                                    titulo.getText().toString(),
                                    tipoServicio.getSelectedItem().toString(),
                                    dateTimeStart,
                                    dateTimeEnd,
                                    checkHoraExtra.isChecked(),
                                    selectedAccount,
                                    cr,
                                    context);

                            if (newEvent != null) {
                                // Evento guardado
                                Toast.makeText(context, "Evento guardado correctamente", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("saved", true);
                                // Datos del evento
                                returnIntent.putExtra("tipo", tipoServicio.getSelectedItem().toString());
                                returnIntent.putExtra("titulo", titulo.getText());
                                returnIntent.putExtra("inicio", dateTimeStart);
                                returnIntent.putExtra("fin", dateTimeEnd);
                                returnIntent.putExtra("id", newEvent.getId());

                                setResult(RESULT_OK, returnIntent);
                                finish();

                            } else {
                                // No guardado
                                //Toast.makeText(context, "Error guardando, posiblemente sin cobertura...", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("saved", false);
                                setResult(RESULT_OK, returnIntent);
                                finish();

                            }

                            //long eventID = Long.parseLong(uri.getLastPathSegment());

                        }


                    });
                }

            // Edición de evento
            } else {

                final String tituloRecibido = intent.getExtras().getString("titulo");
                Log.d(Statics.DEBUG_TAG, "titulo: " + tituloRecibido);

                final String tipoRecibido = intent.getExtras().getString("tipo");
                Log.d(Statics.DEBUG_TAG, "tipo: " + tipoRecibido);

                final long idRecibido = intent.getExtras().getLong("id");
                Log.d(Statics.DEBUG_TAG, "id: " + idRecibido);

                final long fechaInicioRecibida = intent.getExtras().getLong("fecha_inicio");
                Log.d(Statics.DEBUG_TAG, "fecha_inicio: " + fechaInicioRecibida);

                final long fechaFinRecibida = intent.getExtras().getLong("fecha_fin");
                Log.d(Statics.DEBUG_TAG, "fecha_fin: " + fechaFinRecibida);

                final boolean horaExtraRecibida = intent.getExtras().getBoolean("hora_extra");
                Log.d(Statics.DEBUG_TAG, "hora_extra: " + horaExtraRecibida);

                titulo.setText(tituloRecibido);
                checkHoraExtra.setChecked(horaExtraRecibida);
                tipoServicio.setSelection(Utils.getIndexFromStringArray(Arrays.asList(getResources().getStringArray(R.array.array_tipos_eventos)), tipoRecibido));

                Calendar horaInicio = Calendar.getInstance();
                horaInicio.setTimeInMillis(fechaInicioRecibida);
                timePickerInicio.setCurrentHour(horaInicio.get(Calendar.HOUR));
                timePickerInicio.setCurrentMinute(horaInicio.get(Calendar.MINUTE) / 15);
                Log.d(Statics.DEBUG_TAG, "HoraInicio: " + horaInicio.get(Calendar.HOUR));
                Log.d(Statics.DEBUG_TAG, "MinutoInicio: " + horaInicio.get(Calendar.MINUTE));


                Calendar horaFin = Calendar.getInstance();
                horaFin.setTimeInMillis(fechaFinRecibida);
                timePickerFin.setCurrentHour(horaFin.get(Calendar.HOUR));
                timePickerFin.setCurrentMinute(horaFin.get(Calendar.MINUTE) / 15);
                Log.d(Statics.DEBUG_TAG, "HoraFin: " + horaFin.get(Calendar.HOUR));
                Log.d(Statics.DEBUG_TAG, "MinutoFin: " + horaFin.get(Calendar.MINUTE));

                if (acceptButton != null) {
                    acceptButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //GUARDAR

                            /*
                            int valorInicio = timePickerInicio.getCurrentMinute();
                            int valorFin = timePickerFin.getCurrentMinute();

                            Log.i("LIA", "MINUTOS_INI: " + valorInicio);
                            Log.i("LIA", "MINUTOS_FIN: " + valorFin);

                            long dateTimeStart = timePickerToLong(
                                    year,
                                    month,
                                    day,
                                    timePickerInicio.getCurrentHour(),
                                    valorInicio * 15);
                            long dateTimeEnd = timePickerToLong(
                                    year,
                                    month,
                                    day,
                                    timePickerFin.getCurrentHour(),
                                    valorFin * 15);

                            WeekViewEvent newEvent = Server.crearEvento(
                                    titulo.getText().toString(),
                                    tipoServicio.getSelectedItem().toString(),
                                    dateTimeStart,
                                    dateTimeEnd,
                                    selectedAccount,
                                    cr,
                                    context);

                            if (newEvent != null) {
                                // Evento guardado
                                Toast.makeText(context, "Evento guardado correctamente", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("saved", true);
                                // Datos del evento
                                returnIntent.putExtra("tipo", tipoServicio.getSelectedItem().toString());
                                returnIntent.putExtra("titulo", titulo.getText());
                                returnIntent.putExtra("inicio", dateTimeStart);
                                returnIntent.putExtra("fin", dateTimeEnd);
                                returnIntent.putExtra("id", newEvent.getId());

                                setResult(RESULT_OK, returnIntent);
                                finish();

                            } else {
                                // No guardado
                                Toast.makeText(context, "Error guardando, posiblemente sin cobertura...", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra("saved", false);
                                setResult(RESULT_OK, returnIntent);
                                finish();

                            }
*/
                            //long eventID = Long.parseLong(uri.getLastPathSegment());

                        }


                    });
                }




            }

        }catch (Exception e){
            Log.e(Statics.DEBUG_TAG, "Error acceptButton: " + e.toString());
            finish();
        }


    }


}
