package com.liodevel.liacembdr;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.util.*;

import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by emilio on 12/04/16.
 */
public class Server {

    /**
     *
     * @param mes
     * @param anyo
     * @return
     */
    public static List<WeekViewEvent> getEvents(int mes, int anyo, ContentResolver cr, GoogleAccountCredential mCredential, Context context) {

        List<WeekViewEvent> ret = new ArrayList<>();

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(anyo, mes, 1, 0, 0);
        long startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(anyo, mes, 31, 0, 0);
        long endMillis = endTime.getTimeInMillis();
        String valoresExtras = "";

        Cursor cur = null;

        // The ID of the recurring event whose instances you are searching
        // for in the Instances table
        String selection = CalendarContract.Instances.OWNER_ACCOUNT + " = ?";

        String[] selectionArgs = new String[]{mCredential.getSelectedAccountName()};


        //String selection = null;
        //String[] selectionArgs = null;

        // Construct the query with the desired date range.
        Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
        ContentUris.appendId(builder, startMillis);
        ContentUris.appendId(builder, endMillis);

        // Submit the query
        cur = cr.query(builder.build(),
                Statics.INSTANCE_PROJECTION,
                selection,
                selectionArgs,
                null);

        while (cur.moveToNext()) {
            String title = null;
            int color = 0;
            long eventID = 0;
            long beginVal = 0;
            long endVal = 0;

            // Get the field values
            eventID = cur.getLong(Statics.PROJECTION_ID_INDEX);
            beginVal = cur.getLong(Statics.PROJECTION_BEGIN_INDEX);
            endVal = cur.getLong(Statics.PROJECTION_END_INDEX);
            color = cur.getInt(Statics.PROJECTION_EVENT_COLOR);
            title = cur.getString(Statics.PROJECTION_TITLE_INDEX);
            valoresExtras = cur.getString(Statics.PROJECTION_LOCATION);

            // Do something with the values.
            Calendar calendarBegin = Calendar.getInstance();
            calendarBegin.setTimeZone(TimeZone.getDefault());
            calendarBegin.setTimeInMillis(beginVal);
            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.setTimeZone(TimeZone.getDefault());
            calendarEnd.setTimeInMillis(endVal);
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");

            Log.i(Statics.DEBUG_TAG, "Event:  " + title);
            Log.i(Statics.DEBUG_TAG, "Date: " + formatter.format(calendarBegin.getTime()) + " to " + formatter.format(calendarEnd.getTime()));

            // Populate the week view with some events.
            WeekViewEvent event = new WeekViewEvent();
            event.setColor(context.getResources().getColor(R.color.colorSocoExterior));
            event.setName(title);
            event.setStartTime(calendarBegin);
            event.setEndTime(calendarEnd);
            event.setId(eventID);
            event.setColor(color);
            event.setLocation(valoresExtras);

            ret.add(event);

        }

        return ret;
    }


    private static long getCalendarId(ContentResolver cr, String selectionArgs, Context context) {
        String[] projection = new String[]{CalendarContract.Calendars._ID};
        // use the same values as above:

        String selection = CalendarContract.Instances.OWNER_ACCOUNT + " = ?";

        String[] selArgs = new String[]{selectionArgs};

        Log.i(Statics.DEBUG_TAG, "Selected Accounts: " + selection);
        Log.i(Statics.DEBUG_TAG, "Selected Accounts: " + selectionArgs.toString());
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // Si no tiene permisos para LEER CALENDARIO
        }
        Log.i(Statics.DEBUG_TAG, "CR: " + cr.toString());
        Cursor cursor =
                cr.query(
                        CalendarContract.Calendars.CONTENT_URI,
                        projection,
                        selection,
                        selArgs,
                        null);

        while (cursor.moveToNext()) {
            Log.i("LIA", "Calendars: " + cursor.getLong(0));
        }
        if (cursor.moveToFirst()) {
            Log.i("LIA", "Calendars: " + cursor.getLong(0));
            return cursor.getLong(0);
        }
        return -1;
    }


    /**
     * Crea un evento en el servidor
     * @param titulo
     * @param tipoServicio
     * @param dateTimeStart
     * @param dateTimeEnd
     * @return
     */
    public static WeekViewEvent crearEvento(String titulo, String tipoServicio, long dateTimeStart, long dateTimeEnd, boolean horaExtra, String selection, ContentResolver cr, Context context) {
        long calId = getCalendarId(cr, selection, context);
        // El mes empieza en 0


        WeekViewEvent event = null;

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, dateTimeStart);
        values.put(CalendarContract.Events.DTEND, dateTimeEnd);
        values.put(CalendarContract.Events.TITLE, tipoServicio + "(" + titulo + ")");
        values.put(CalendarContract.Events.CALENDAR_ID, calId);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "Europe/Berlin");
        if (tipoServicio.equals("SOCO")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.RED);
        } else if (tipoServicio.equals("SOCO EXTERIOR")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.rgb(0,100,0));
        } else if (tipoServicio.equals("CURSILLO")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.BLUE);
        } else if (tipoServicio.equals("EP")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.BLACK);
        } else if (tipoServicio.equals("MAINADERA")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.MAGENTA);
        } else if (tipoServicio.equals("REUNIÓN")) {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.DKGRAY);
        } else {
            values.put(CalendarContract.Events.EVENT_COLOR, Color.DKGRAY);
        }
        values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
        values.put(CalendarContract.Events.ALL_DAY, 0);
        if (horaExtra){
            values.put(CalendarContract.Events.EVENT_LOCATION, "X");
        } else {
            values.put(CalendarContract.Events.EVENT_LOCATION, "");
        }

        //values.put(CalendarContract.Events.ALLOWED_REMINDERS, 0);

        //values.put(CalendarContract.Events.GUESTS_CAN_INVITE_OTHERS, 1);
        //values.put(CalendarContract.Events.GUESTS_CAN_MODIFY, 1);
        //values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        //values.put(CalendarContract.Events.ORGANIZER, "some.mail@some.address.com");
        //values.put(CalendarContract.Events.RRULE, "FREQ=DAILY;COUNT=20;BYDAY=MO,TU,WE,TH,FR;WKST=MO");
        //values.put(CalendarContract.Events.EVENT_LOCATION, "Münster");
        //values.put(CalendarContract.Events.DESCRIPTION, "The agenda or some description of the event");
        //values.put(CalendarContract.Events.SELF_ATTENDEE_STATUS, CalendarContract.Events.STATUS_CONFIRMED);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Avisar de problema con permisos
            return null;
        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        try {
            long eventId = new Long(uri.getLastPathSegment());
            Log.i("LIA", "Evento guardado");

            event = new WeekViewEvent();
            event.setColor(context.getResources().getColor(R.color.colorSocoExterior));
            event.setName(tipoServicio + "(" + titulo + ")");
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTimeInMillis(dateTimeStart);
            event.setStartTime(calendarStart);
            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.setTimeInMillis(dateTimeEnd);
            event.setEndTime(calendarEnd);
            event.setId(eventId);
            event.setColor((int) values.get(CalendarContract.Events.EVENT_COLOR_KEY));


        } catch (Exception e) {
            Toast.makeText(context, "Error, evento NO guardado", Toast.LENGTH_SHORT).show();
        }


        return event;
    }


    /**
     * Borra un evento del servidor
     * @param id
     * @param cr
     * @param context
     * @return
     */
    public static boolean borrarEvento(long id, ContentResolver cr, Context context) {
        boolean ret = false;
        String[] selArgs = new String[]{Long.toString(id)};
        Log.d(Statics.DEBUG_TAG, "BORRAR: " + selArgs[0].toString());

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        int deleted = cr.delete(CalendarContract.Events.CONTENT_URI, CalendarContract.Events._ID + " =? ", selArgs);
        Log.d(Statics.DEBUG_TAG, "BORRADO: " + deleted);

        if (deleted == 1) {
            ret = true;
        }
        return ret;
    }



}
