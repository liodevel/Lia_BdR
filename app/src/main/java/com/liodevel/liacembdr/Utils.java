package com.liodevel.liacembdr;

import android.util.Log;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import com.alamkanak.weekview.WeekViewEvent;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by emilio on 7/04/16.
 */
public class Utils {


    /**
     *
     * @param year
     * @param month
     * @param day
     * @param hour
     * @param minute
     * @return
     */
    static public long timePickerToLong (int year, int month, int day, int hour, int minute){
        Calendar calendar = new GregorianCalendar(year, month, day, hour, minute);
        Log.i("LIA", "Fecha: " + calendar.toString());
        return calendar.getTimeInMillis();

    }


    static public long getIndexById(long eventId, List<WeekViewEvent> events){
        long ret = -1;
        long index = 0;

        for (WeekViewEvent event:events){
            if (eventId == event.getId()){
                ret = index;
            }
            index++;
        }

        return ret;
    }

    /**
     * devuelve el index de un Array de Strings
     * @param stringArray
     * @param text
     * @return
     */
    static public int getIndexFromStringArray(List<String> stringArray, String text){
        int ret = stringArray.indexOf(text);
        return ret;
    }

}
